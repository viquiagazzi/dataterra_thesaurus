## Data Terra thesaurus specifications

Sources : 
+ [sosa ](http://www.w3.org/ns/sosa/) ontology
+ [skos ](http://www.w3.org/2004/02/skos/core#) ontology
+ [ssn ](http://www.w3.org/ns/ssn/) ontology 

M : Mandatory, R : Recommended, O : Optional

# Classes

Class               | Type Data Terra     | IRI                     |
--------------------|---------------------|-------------------------|
ConceptScheme       | M                   | skos:ConceptScheme      |
Concept             | M                   | skos:Concept            |
Feature of Interest | M                   | sosa:FeatureOfInterest  |
ObservableProperty  | M                   | sosa:ObservableProperty |
Sensor              | M                   | sosa:Sensor             |
Platform            | M                   | sosa:Platform           |

# Common properties for all concepts

Property        | Type Data Terra | Range              | IRI             |
----------------|-----------------|--------------------|-----------------|
skos:prefLabel  | M               | rdfs:Literal       | skos:prefLabel  |
skos:definition | M               | rdfs:Literal       | skos:definition |
skos:altLabel   | O               | rdfs:Literal       | skos:altLabel   |
skos:inScheme   | M               | owl:ObjectProperty | skos:inScheme   |
skos:exactMatch | M               | owl:ObjectProperty | skos:exactMatch |
skos:broader    | O               | owl:ObjectProperty | skos:broader    | 
skos:narrower   | O               | owl:ObjectProperty | skos:narrower   |


# Features of Interest properties
Feature of interest : the thing whose property is being estimated or calculated in the course of an Observation to arrive at a Result, or whose property is being manipulated by an Actuator, or which is being sampled or transformed in an act of Sampling.

Property        | Type Data Terra | Range              | IRI              |
----------------|-----------------|--------------------|------------------|
ssn:hasProperty | R               | owl:ObjectProperty | ssn:hasProperty  |

# Variables properties
Observable Property : an observable quality (property, characteristic) of a FeatureOfInterest.

Property          | Type Data Terra | Range              | IRI               |
------------------|-----------------|--------------------|-------------------|
ssn:isPropertyOf  | R               | owl:ObjectProperty | ssn:isPropertyOf  |
sosa:isObservedBy | R               | sosa:Sensor        | sosa:isObservedBy |

# Sensors properties
Sensor : device, agent (including humans), or software (simulation) involved in, or implementing, a Procedure. Sensors respond to a Stimulus, e.g., a change in the environment, or Input data composed from the Results of prior Observations, and generate a Result. Sensors can be hosted by Platforms.

Property         | Type Data Terra | Range                   | IRI              |
-----------------|-----------------|-------------------------|------------------|
sosa:observes    | R               | sosa:ObservableProperty | sosa:observes    |

# Platforms properties
Platform : a Platform is an entity that hosts other entities, particularly Sensors, Actuators, Samplers, and other Platforms.

Property         | Type Data Terra | Range                      | IRI              |
-----------------|-----------------|----------------------------|------------------|
sosa:hosts       | R               | sosa:Sensor, sosa:Platform | sosa:hosts       |

