# Contexte 

L'outil LDRegistry est celle qui hebergera les vocabulaires controlées pour les 2 registres suivants :
	+ Vocabulaires colonne vertébrale IR Data Terra : sous https://terra-vocabulary.org/ncl/_FAIR-Incubator
	+ Vocabulaires repository IR Data Terra : sous https://terra-vocabulary.org/ncl/_DataTerraRepositoryFairIncubator

Les vocabulaires sont exposés par LDRegistry. Pour leurs création et mis à jour, l'outil VocBench3 est utilisé en amont de sa publication dans LDRegistry. 
 
# Objectifs

Dans ce contexte il est donc nécéssaire de tester la chaîne de manipulations à réaliser pour passer d'un outil à l'autre, selon les différents cas de mis à jour, afin d'identifier une procédure la plus simple, directe et robuste possible.
Dans un premier temps, pour les vocabulaires liés à la colonne vertébrale Data Terra, un test de upload d'un registre (scheme + concepts) est effectué. Il s'agit du vocabulaire des Features Of Interest' Types (https://terra-vocabulary.org/ncl/FAIR-Incubator/earthfeaturetype). 

# Séquence des manipulations dans registre  https://terra-vocabulary.org/ncl/_FAIR-Incubator

## Avant d'importer un fichier avec un thésaurus déjà créé

+ Changer l'URI du ConceptScheme soit avant l'import dans VocBench, soit à la main une fois les autres modifications effectués dans VocBench --> <https://terra-vocabulary.org/ncl/FAIR-Incubator/earthfeaturetype> (sans '/' à la fin)

## Modifications à apporter au graphe dans VocBench : Creer un nouveau projet VocBench avec thésaurus FOItypes de base. 

+ Changer uri de base : https://terra-vocabulary.org/ncl/FAIR-Incubator/earthfeaturetype/
+ Import des ontologies : reg:, ldp:, registry-ui:
	+ :reg (https://raw.githubusercontent.com/ukgovld/registry-core/master/src/main/vocabs/registryVocab.ttl) à télécharger en local en amont pour l'importer. 
	+ :ldp (http://www.w3.org/ns/ldp#) si l'import échou --> il faut créér à la main la classe ldp:Container (sous-classe owl:Thing) à partir de l'uri http://www.w3.org/ns/ldp#Container.
	+ :registry-ui (https://raw.githubusercontent.com/UKGovLD/registry-core/master/src/main/vocabs/ui.ttl) l'import échoué; à télécharger en local en amont pour l'importer.

#### Modifications pour le ConceptScheme (reg:Register). 

+ Type: skos:ConceptScheme , reg:Register , ldp:Container
+ rdfs:label identique au skos:prefLabel
+ rdfs:member pour tous les concepts contenus dans le registre (y compris les TopConcepts)
	+ INSERT{    
		GRAPH <{BaseUri du graph}> {<{Uri ConceptScheme}> rdfs:member ?s .}
		} WHERE {
		?s a skos:Concept .
		?s skos:inScheme <{Uri ConceptScheme}>.}    
		
+ dct:description ---> ça passe sans à l'upload
+ reg:inverseMembershipPredicate  skos:inScheme ;
+ reg:notation                    "earthfeaturetypes" ;
+ registry-ui:hierarchyChildProperty       skos:narrower ; ---> besoin pour affichage de l'arbo skos
+ registry-ui:hierarchyRootProperty        skos:topConceptOf ; ---> besoin pour affichage de l'arbo skos
+ ldp:hasMemberRelation           rdfs:member ; ---> ça passe sans à l'upload
+ ldp:isMemberOfRelation          skos:inScheme ; ---> ça passe sans à l'upload (selon Elsa)
+ reg:license ---> avec dct:license ça passe à l'upload
+ owl:versionInfo 1 ; ---> incrémenté auto à l'upload

#### Modifications pour les Concept (reg:Entity)

+ création skos:notation "c_b1bee603" ; ---> ça passe sans à l'upload
+ adms:status  <https://inspire.ec.europa.eu/registry/status/submitted> ; ---> ça passe sans à l'upload
+ rdfs:label identique au skos:prefLabel
	+ INSERT{        
		GRAPH <{BaseUri du graph}> { ?s rdfs:label ?prefLabel .}        
		} WHERE {    
		?s skos:prefLabel ?prefLabel .        
		}
		
+ dct:description ---> ça passe sans à l'upload

#### Modifications pour les Collections (selon exemple [EPOS incubator](https://registry.epos-eu.org/ncl/FAIR-Incubator/tcs-GIM) )

+ Typage avec skos:Collection , reg:Register , ldp:Container
+ rdfs:label identique au skos:prefLabel
+ reg:notation "skosCollection_bcf3cc61" ;
+ skos:member :c_13d46653, :skosCollection_0a254ba7 ; --> pour les concepts et collections imbriqués
+ reg:subregister :skosCollection_0a254ba7 ; --> pour les registre imbriqués (skos:Collection ou skos:ConceptScheme)
+ owl:versionInfo 1 ;
+ ldp:hasMemberRelation skos:member ;

## Modifications à la main (hors VocBench)

+ Export du .ttl de VB
+ Vérification pour l'URI du ConceptScheme, et les URIs des skos:inscheme, skos:topConceptOf des concepts (si ceci n'a pas était fait en amont). 
+ Vérification pour les prefixes définis dans le .ttl (pour sosa, ssn, etc) : pour ne pas trainer des URI longues pour les types de chaque objet. 
+ Supprimer les éléments qui définissent l’ontologie associé à la baseURI créé par défault dans VocBench3. Suppression des owl:imports également. 
+ Supprimer toutes les sous-classes ou propriétés qu’on a créé dans Vocbench qui sont déjà gérées dans LDRegistry : ldp:Container ou autres. 
-- (+) Changer les URIs de base des skos:exactMatch des allignements GCMD car maj côté NASA mars 2022

## Uploader le graphe

+ importer directement tout le fichier ttl : Actions → Registry new → contains a sub-register plus its contents ("bulk upload")

## Changement de statut Submitted ---> Experimental (afin de voir le registre uploadé sans authentication dans LDRegistry)

+ à partir de l'interface Actions / Set status or Set contents status

## Update d'un concept déjà présent dans le registre (un TopConcept)

## Update du registre (skos:ConceptScheme) : 

+ Ajout manuel d'autres prédicats à la main en passant par l'interface graphique LDRegistry : OK
+ Ajout d'un nouveau TopConcept ?


# Problèmes identifiés

+ Manque de compatibilité entre les 2 façons de gérer le contenu d'un Registry (LDRegistry) et un projet (VocBench3) ? Chaque projet VocBench3 peut contenir plusieurs schemes, est-ce le cas pour LDRegistry ? car l'outil attends que les URIs des Concepts soient formés à partir de l'URI du Registre. 

+ Les manipulations entrainnent des mises à jour dans VocBench, avec des modifs ultérieures dans un éditeur de texte. Une fois le Registre uploadé, besoin de télécharger la dernière version à partir de LDReistry pour la modifier à nouveau (incrément de version interne à l'outil, modifications mineures apportés) et ceci ne peut pas être directement importé dans VocBench à nouveau. Chaîne peu fluide / robuste.

+ Du point de vue d'une maintenance distribué à plusieurs utilisateurs / référents des vocabulaires, il existe un besoin de pouvoir suivre les avancements de chaque vocabulaire. Ceci est en partie possible en interne car LDRegistry a un historique des updates. 

