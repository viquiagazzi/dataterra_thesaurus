#!/bin/bash
# $1 input_file

#sed 's|skos:Concept\b|<http://www.w3.org/2004/02/skos/core#Concept>|g' $1 > output.ttl #'\b' for boundary character
#sed -i 's|skos:FeatureOfInterest|<http://www.w3.org/2004/02/skos/core#FeatureOfInterest>|g' output.ttl
#sed -i 's|skos:inScheme|<http://www.w3.org/2004/02/skos/core#inScheme>|g' output.ttl

sed 's|skos:Concept\b|<http://www.w3.org/2004/02/skos/core#Concept>|g  #'\b' for boundary character
		s|sosa:ObservableProperty|<http://www.w3.org/ns/sosa/ObservableProperty>|g
		s|skos:topConceptOf|<http://www.w3.org/2004/02/skos/core#topConceptOf>|g
		s|skos:hasTopConcept|<http://www.w3.org/2004/02/skos/core#hasTopConcept>|g
		s|skos:inScheme|<http://www.w3.org/2004/02/skos/core#inScheme>|g
		s|:conceptScheme_d30673d8|<http://www.data-terra.org/concept/concept_scheme/EarthScienceProperty>|g
		s|<https://terra-vocabulary.org/ncl/earthvariables>|<http://www.data-terra.org/concept/concept_scheme/EarthScienceProperty>|g
		s|skos:prefLabel|<http://www.w3.org/2004/02/skos/core#prefLabel>|g
		s|skos:altLabel|<http://www.w3.org/2004/02/skos/core#altLabel>|g
		s|skos:broader|<http://www.w3.org/2004/02/skos/core#broader>|g
		s|skos:narrower|<http://www.w3.org/2004/02/skos/core#narrower>|g
		s|skos:definition|<http://www.w3.org/2004/02/skos/core#definition>|g
		s|skos:exactMatch|<http://www.w3.org/2004/02/skos/core#exactMatch>|g
		s|skos:closeMatch|<http://www.w3.org/2004/02/skos/core#closeMatch>|g
		s|skos:relatedMatch|<http://www.w3.org/2004/02/skos/core#relatedMatch>|g
		s|skos:narrowMatch|<http://www.w3.org/2004/02/skos/core#narrowMatch>|g
		s|ssn:isPropertyOf|<http://www.w3.org/ns/ssn/isPropertyOf>|g
		s|skos:Collection|<http://www.w3.org/2004/02/skos/core#Collection>|g
		s|skos:member|<http://www.w3.org/2004/02/skos/core#member>|g
		s|:skosCollection_\([a-zA-Z0-9]\{8\}\)\b|<https://terra-vocabulary.org/ncl/earthvariables/skosCollection_\1>|g
		s|dct:license|<http://purl.org/dc/terms/license>|g
		s|dct:title|<http://purl.org/dc/terms/title>|g
		s|dct:created|<http://purl.org/dc/terms/created>|g
		s|dct:description|<http://purl.org/dc/terms/description>|g
		s|owl:versionInfo|<http://www.w3.org/2002/07/owl#versionInfo>|g
		s|xsd:anyURI|<https://www.w3.org/TR/xmlschema11-2/#anyURI>|g
		s|xsd:date|<https://www.w3.org/TR/xmlschema11-2/#date>|g
		s|rdf:HTML|<www.w3.org/1999/02/22-rdf-syntax-ns#HTML>|g		
		s|:c_\([a-zA-Z0-9]\{8\}\)\b|<https://terra-vocabulary.org/ncl/earthvariables/c_\1>|g' $1 > output.ttl # '\1' for 1st matched group. Thésaurus Variables.
