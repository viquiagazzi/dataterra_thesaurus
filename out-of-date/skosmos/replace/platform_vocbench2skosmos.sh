#!/bin/bash
# $1 input_file

#sed 's|skos:Concept\b|<http://www.w3.org/2004/02/skos/core#Concept>|g' $1 > output.ttl #'\b' for boundary character
#sed -i 's|skos:FeatureOfInterest|<http://www.w3.org/2004/02/skos/core#FeatureOfInterest>|g' output.ttl
#sed -i 's|skos:inScheme|<http://www.w3.org/2004/02/skos/core#inScheme>|g' output.ttl

sed 's|skos:Concept\b|<http://www.w3.org/2004/02/skos/core#Concept>|g  #'\b' for boundary character
		s|sosa:Platform|<http://www.w3.org/ns/sosa/Platform>|g
		s|skos:topConceptOf|<http://www.w3.org/2004/02/skos/core#topConceptOf>|g
		s|skos:hasTopConcept|<http://www.w3.org/2004/02/skos/core#hasTopConcept>|g
		s|skos:inScheme|<http://www.w3.org/2004/02/skos/core#inScheme>|g
		s|:scienceplatforms|<http://www.data-terra.org/concept/concept_scheme/EarthSciencePlatform>|g
		s|skos:prefLabel|<http://www.w3.org/2004/02/skos/core#prefLabel>|g
		s|skos:altLabel|<http://www.w3.org/2004/02/skos/core#altLabel>|g
		s|skos:broader|<http://www.w3.org/2004/02/skos/core#broader>|g
		s|skos:narrower|<http://www.w3.org/2004/02/skos/core#narrower>|g
		s|skos:definition|<http://www.w3.org/2004/02/skos/core#definition>|g
		s|skos:exactMatch|<http://www.w3.org/2004/02/skos/core#exactMatch>|g
		s|skos:relatedMatch|<http://www.w3.org/2004/02/skos/core#relatedMatch>|g
		s|skos:Collection|<http://www.w3.org/2004/02/skos/core#Collection>|g
		s|skos:member|<http://www.w3.org/2004/02/skos/core#member>|g
		s|:c_\([a-zA-Z0-9]\{8\}\)\b|<https://terra-vocabulary.org/ncl/scienceplatforms/c_\1>|g' $1 > output.ttl # '\1' for 1st matched group. Thésaurus Platforms.
	

