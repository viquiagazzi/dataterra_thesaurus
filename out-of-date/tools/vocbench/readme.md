# Guidelines' Vocbench content

## Intro

+ Web officiel du projet : http://vocbench.uniroma2.it/
	+ Tool for managing ontologies / thesaurus / controlled vocabularies : [OWL](https://www.w3.org/TR/owl2-overview/), [SKOS(XL)](https://www.w3.org/TR/2009/REC-skos-reference-20090818/), [RDF](https://www.w3.org/TR/rdf-schema/) datasets
	+ Developped by EU : http://publications.europa.eu/ by University of Rome Tor Vergata	 
	+ Download V3 : https://bitbucket.org/art-uniroma2/vocbench3/downloads/
	+ Code source : https://bitbucket.org/art-uniroma2/vocbench3/src/master/
+ Guidelines 
	+ Installation : http://vocbench.uniroma2.it/doc/#installation : dependencies on Apache Karaf server, Semantic Turkey server and Java8.
	+ Docker install (**beware of data storage !**) : https://bitbucket.org/art-uniroma2/vocbench3-docker/src/master/
	+ User manual : http://vocbench.uniroma2.it/doc/user/
	+ SKOS dev manual : http://vocbench.uniroma2.it/doc/skos.jsf
	+ Video tutorial focused on SKOS vocabulary : https://www.youtube.com/watch?v=IljCs5RobgI
	+ Support : https://groups.google.com/g/vocbench-user
	
## Test drive for creating a SKOS project with a small thésaurus (from scratch)

+ http://vocbench.uniroma2.it/doc/user/test_drive.jsf#creating_a_skos_project_for_managing_a_small_thesaurus

## Step by step example based on Data Terra vocabulary

+ Download Data Terra thesaurus file [**Features of Interest Type**](https://skosmos.geomatys.com/earthsciencefeature/en/) : https://gitlab.com/viquiagazzi/dataterra_thesaurus/-/tree/main/features_of_interest_types

+ New project. Import from local file in VocBench. Optional settings : URI generator. 

+ Projects' properties : table and property menu. 

+ Data tab : Class, Concept, Scheme, Collection, Property, Datatype. 

+ Concept tab : 
	+ Selection of the existing ConceptScheme
	+ TopConcepts, browse tree view
	+ For concept 'Land surface' identify all the properties in ResView and Code. 
	
+ Metadata / Namespaces and imports
	+ Refactoring base URIs and/ or namespaces imported. 
	+ Import other ontologies : [ssn](https://www.w3.org/TR/vocab-ssn/), [sosa](https://www.w3.org/TR/vocab-ssn/#Modularization). 

+ From the Concept tab, create the concept 'Land surface / Cropland'.
	+ :c_f5cd6409 a skos:Concept, <http://www.w3.org/ns/sosa/FeatureOfInterest> ;
 	+ skos:prefLabel "Cropland"@en;
 	+ skos:definition "Pertaining to areas used for the production of adapted crops for harvest."@en ;
  	+ skos:inScheme <https://terra-vocabulary.org/ncl/earthfeaturetype> ;
  	+ skos:broader :c_46fa2569, :c_2257161a ;
  	+ skos:exactMatch <https://gcmd.earthdata.nasa.gov/kms/concept/2c74f390-9d82-4903-98e0-bddf0d3247fb> .
  
+ Create a Collection (skos:Collection)
	+ From the Collection tab, create a new collection called 'Ecosystem_col'
	+ Add members with **multiselection tool** : 'Biosphere / Ecosystems' and 'Oceans / Marine ecosystems / Tropical'
	+ Verify code result on the Code tab. 

+ Sparql editor. Use case : add one new property for each element of the project.
	+ Sparql page, query tab for standard sparql queries.
	+ 3 parts : 
		+ text area for query editing
		+ central part for clearing/submitting the query, an exporter for saving the results and for internal storing/loading queries
		+ bottom part reports the results of the query : tuples results (SELECT), graph results (CONSTRUCT), none (INSERT). 
	+ Test sparql query for adding a rdfs:label for each skos:concept : https://gitlab.com/viquiagazzi/dataterra_thesaurus/-/blob/main/tools/vocbench/query/rdfs_label_for_skos_concept.rq
	+ Results' verification on code tab for any concept on the graph.
	
+ Export project's content to local file : turtle or other RDF serialisation. 

## Data Terra thesaurus model (**under construction !**)

+ Vocabularies specifications : https://gitlab.com/viquiagazzi/dataterra_thesaurus/-/blob/main/model/classes_properties_thesaurus.md

+ ConceptScheme definition : https://gitlab.com/viquiagazzi/dataterra_thesaurus/-/blob/main/model/data_terra_thesaurus_metadata.ttl 

## Other references

### W3C Recommendations ans specifications 

+ SKOS primer guide : https://www.w3.org/TR/skos-primer/
+ SSN : https://www.w3.org/TR/vocab-ssn/
+ SOSA : http://www.w3.org/ns/sosa

### Data Terra vocabularies recommendations

+ Vocabularies repository (**under construction !**) : https://gitlab.com/viquiagazzi/dataterra_thesaurus
